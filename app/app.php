<?php

namespace app;

include_once 'components/config.php';
include_once 'components/routing.php';
include_once 'components/system.php';
include_once 'components/database.php';
include_once 'components/authorization.php';
include_once 'components/translation.php';

include_once './app/helper/actions.php';

use \app\helper\actions as actions;


class app
{
  public $BasePath;
  public $BaseURL;
  public $RequestMethod;
  public $Action;
  public $Parameter;
  public $Router;
  public $Sys;
  //public $Config;
  public $DB;
  public $Auth;
  public $t;

  public $Request;
  public $Data;

  public function setup($AuthToken = '')
  {
      
    $this->t = new \app\components\translation();
    $this->DB = new \app\components\database();
    //$this->Config = new \app\components\config();
    /*if ($AuthToken == '') {
      die($this->t('no_acc_param'));
    }*/
    $this->URLs();
      
    $this->Router = new \app\components\router();

    $this->Sys = new \app\components\system();

    $this->Auth = new \app\components\authorization();

    $this->Auth->setToken($AuthToken);
  }
  public function t($String)
  {
    return $this->t->translate($String);
  }
  public function route_request()
  {
    return $this->Router->route();
  }
  private function getAction()
  {
    $this->Action = str_replace($this->BaseURL, '', $_SERVER['REQUEST_URI']);
    if (strstr($this->Action, 'index.php')) $this->Action = str_replace('index.php', '', $this->Action);
    if (strlen($this->Action) == 0) {
      die($this->t('no_action'));
    } else {
      if (strstr($this->Action, '?')) {
        if (isset($_GET['action'])) {
          $this->Action = actions::execute($_GET['action']);
        }
        $this->Action = substr($this->Action, 0, strpos($this->Action, '?'));
      }
      $this->Action = str_replace(strtolower($this->BaseURL), '', strtolower($this->Action));
      if (strpos($this->Action, '/') > 0) {
        $this->Parameter = strstr($this->Action, '/');
        $this->Action = str_replace($this->Parameter, '', $this->Action);
      }
      if (isset($_POST['request'])) {
        $this->Request = $_POST['request'];
        $this->Data = array();
        if (isset($_POST['data'])) {
          $this->Data = $_POST['data'];
        }
      }
    }
    if (substr($this->Action, 0, 1) == "/") {
      $this->Action = substr($this->Action, 1);
    }
  }
  private function URLs()
  {
    $this->RequestMethod = $_SERVER['REQUEST_METHOD'];
    $this->BasePath = (strstr($_SERVER['SCRIPT_FILENAME'], 'index.php')) ? str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) : $_SERVER['SCRIPT_FILENAME'];
    $this->BaseURL = (strstr($_SERVER['SCRIPT_NAME'], 'index.php')) ? str_replace('index.php', '', $_SERVER['SCRIPT_NAME']) : $_SERVER['SCRIPT_NAME'];
    $this->getAction();
  }
}
