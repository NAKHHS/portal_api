<?php

namespace app\components;

class config
{
  private $larrConfig;
  private $lstrModule;

  public function __construct()
  {
    $this->larrConfig = json_decode(file_get_contents('./config/app.json'), true);
    $this->lstrModule = 'app';
  }
  private function checkFile($Module = null)
  {
    if ($Module == null) {
      if ($this->lstrModule != 'app') {
        $this->larrConfig = json_decode(file_get_contents('./config/app.json'));
        $this->lstrModule = 'app';
      }
    } else {
      if ($this->lstrModule != $Module) {
        $this->larrConfig = json_decode(file_get_contents('./config/'.strtolower($Module).'.json'), true);
        $this->lstrModule = strtolower($Module);
      }
    }
  }

  public function set($Path, $Value, $Module = null)
  {
    $this->checkFile($Module);
      if (strstr($Path, ':')) {
        $larrPath = explode(':', $Path);
        switch (count($larrPath)) {
          case 1:
            $this->larrConfig[$larrPath[0]] = $Value;
            break;
          case 2:
            $this->larrConfig[$larrPath[0]][$larrPath[1]] = $Value;
            break;
          case 3:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]] = $Value;
            break;
          case 4:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]] = $Value;
            break;
          case 5:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]] = $Value;
            break;
          case 6:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]] = $Value;
            break;
          case 7:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]] = $Value;
            break;
          case 9:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]] = $Value;
            break;
          case 9:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]][$larrPath[8]] = $Value;
            break;
          case 10:
            $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]][$larrPath[8]][$larrPath[9]] = $Value;
            break;
          default:
            echo ('Der Pfad hat eine zu große Tiefe, es dürfen maximal 10 Unterschlüssel zu finden sein.');
            return false;
        }
      } else {
        $this->larrConfig[$Path] = $Value;
      }
      file_put_contents('./config/'.$this->lstrModule.'.json',json_encode($this->larrConfig));
  }
  public function get($Path, $Module = null)
  {
    $this->checkFile($Module);
    if (strstr($Path, ':')) {
      $larrPath = explode(':', $Path);
      switch (count($larrPath)) {
        case 1:
          return $this->larrConfig[$larrPath[0]];
        case 2:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]];
        case 3:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]];
        case 4:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]];
        case 5:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]];
        case 6:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]];
        case 7:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]];
        case 9:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]];
        case 9:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]][$larrPath[8]];
        case 10:
          return $this->larrConfig[$larrPath[0]][$larrPath[1]][$larrPath[2]][$larrPath[3]][$larrPath[4]][$larrPath[5]][$larrPath[6]][$larrPath[7]][$larrPath[8]][$larrPath[9]];
        default:
          echo ('Der Pfad hat eine zu große Tiefe, es dürfen maximal 10 Unterschlüssel zu finden sein.');
          return false;
      }
    } else {
      return $this->larrConfig[$Path];
    }
  }
}
