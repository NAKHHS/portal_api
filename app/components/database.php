<?php

namespace app\components;

include_once './app/helper/strings.php';

use \app\helper\strings as strings;

class database
{

  private $larrConnectionParameters;

  private $lobjConnection;
  private $Table;
  private $Columns;
  private $Where;
  private $OrderBy;
  private $Limit;
  private $Query;

  public function __construct()
  { 
    $this->larrConnectionParameters = include_once('./config/database.php');
  }

  public function new()
  {
    $this->Query = null;
    $this->Table = null;
    $this->Where = null;
    $this->Columns = null;
    $this->Limit = null;
  }

  private function preCheck()
  {
    global $app;
    if ($this->lobjConnection == null) {
      $this->lobjConnection = new \mysqli(
        $this->larrConnectionParameters['host'], 
        $this->larrConnectionParameters['user'], 
        $this->larrConnectionParameters['password'], 
        $this->larrConnectionParameters['database']
      );
      if ($this->lobjConnection->connect_errno) {
        echo "Probleme bei der Verbindung zur Datenbank";
        echo "Fehlernummer: " . $this->lobjConnection->connect_errno . "\n";
        echo "Fehler: " . $this->lobjConnection->connect_error . "\n";
        exit;
      }
      $this->lobjConnection->query("SET NAMES 'utf8'");
    }
  }

  private function lastCheck()
  {
    global $app;
    if (!$this->larrConnectionParameters['permanent']) {
      $this->lobjConnection = null;
    }
  }

  public function exec($Query = null, $Return = '')
  {
    return $this->execute($Query, $Return);
  }

  public function execute($Query = null, $Return = '')
  {
    if ($Query == null) {
      $Query = $this->Query;
    }
    $this->preCheck();
    $lobjResult = $this->lobjConnection->query($Query);

    $larrReturn = array();
    switch ($Return) {
      case 'all':
        $larrReturn['query'] = $Query;
        if ($lobjResult) {
          $larrReturn['returnObject'] = array();
          $larrReturn['returnObject']['current_field'] = (isset($lobjResult->current_field)) ? $lobjResult->current_field : 'not_set';
          $larrReturn['returnObject']['field_count'] = (isset($lobjResult->field_count)) ? $lobjResult->field_count : 'not_set';
          $larrReturn['returnObject']['lengths'] = (isset($lobjResult->lengths)) ? $lobjResult->lengths : 'not_set';
          $larrReturn['returnObject']['num_rows'] = (isset($lobjResult->num_rows)) ? $lobjResult->num_rows : 'not_set';
          $larrReturn['returnObject']['type'] = (isset($lobjResult->type)) ? $lobjResult->type : 'not_set';
          if (isset($lobjResult->num_rows) && $lobjResult->num_rows > 0) {
            $larrReturn['Data'] = mysqli_fetch_all($lobjResult, MYSQLI_ASSOC);
          }
          return $larrReturn;
        }
        break;
      case 'id':
        return $this->lobjConnection->insert_id;
        break;
      case 'success':
        if ($lobjResult) {
          return 'true';
        } else {
          return 'false';
        }
        break;
      case '':
        if (isset($lobjResult->num_rows) && $lobjResult->num_rows > 0) {
          if ($lobjResult->num_rows > 0) {
            return mysqli_fetch_all($lobjResult, MYSQLI_ASSOC);
          } else {
            return array('message' => 'no_data_returned', 'error' => $this->lobjConnection->error);
          }
        }
        break;
    }

    $this->lastCheck();
  }

  // ++++++++++++++++++++++++++++++ FIELDS
  public function Query($Query)
  {
    $this->new();
    $this->Query = $Query;
    return $this;
  }

  public function Where($Where)
  {
    $this->Where = $Where;
    return $this;
  }

  public function Columns($Columns)
  {
    $this->Columns = $Columns;
    return $this;
  }

  public function Table($Table)
  {
    $this->new();
    $this->Table = $Table;
    return $this;
  }

  public function OrderBy($OrderBy)
  {
    $this->OrderBy = $OrderBy;
    return $this;
  }

  public function Limit($Limit)
  {
    $this->Limit = $Limit;
    return $this;
  }

  private function checkTypeForWhere($Value)
  {
    switch (gettype($Value)) {
      case 'string':
        if (strstr($Value, 's:')) {
          return "'" . str_replace('s:', '', $Value) . "'";
        } else {
          return $Value;
        }

        // no break
      default:
        return $Value;
    }
  }

  public function empty($Table)
  {
    $this->execute("TRUNCATE TABLE " . $Table);
  }

  // +++++++++++++++++++++++++++ BUILD
  private function whereClause($Data)
  {
    $lstrQuery = ' WHERE ';
    foreach ($Data as $Column => $Value) {
      $lstrQuery .= $Column . "'" . $Value . "', ";
    }
    return strings::shorten($lstrQuery, 2);
  }

  private function updateDataClause($Data)
  {
    $lstrValues = ' SET ';
    foreach ($Data as $Column => $Value) {
      $lstrValues .= $Column . " = '" . $Value . "', ";
    }
    return strings::shorten($lstrValues, 2);
  }

  public function insertClause($Table, $Data)
  {
    $lstrSQL = "INSERT INTO `" . $Table . "` ";
    $lstrColumns = "";
    $lstrValues = "";
    foreach ($Data as $lstrColumn => $lstrValue) {
      $lstrColumns .= $lstrColumn . ", ";
      $lstrValues .= "'" . $lstrValue . "', ";
    }
    $lstrSQL .= "(" . strings::shorten($lstrColumns, "2") . ") VALUES (" . \strings::shorten($lstrValues, 2) . ")";

    return $this->execute($lstrSQL, 'id');
  }

  private function buildWhere($Where)
  {
    $lstrQuery = '';
    if ($Where != null) {
      if (is_array($Where)) {
        $lstrQuery .= ' WHERE ';
        foreach ($Where as $larrWhere) {
          switch (count($larrWhere)) {
            case 1:
              $lstrQuery .= $this->checkTypeForWhere($larrWhere) . ' AND ';
              break;
            case 2:
              $lstrQuery .= $larrWhere[0] . ' = ' . $this->checkTypeForWhere($larrWhere[1]) . ' AND ';
              break;
            case 3:
              $lstrQuery .= $larrWhere[0] . ' ' . $larrWhere[1] . ' ' . $this->checkTypeForWhere($larrWhere[2]) . ' AND ';
              break;
          }
        }
        $lstrQuery = strings::shorten($lstrQuery, 5);
      } else {
        $lstrQuery .= ' WHERE ' . $Where;
      }
    }
    return $lstrQuery;
  }

  private function buildOrderBy($OrderBy)
  {
    $lstrQuery = '';
    if ($OrderBy != null) {
      if (is_array($OrderBy)) {
        foreach ($OrderBy as $lstrOrderBy) {
          $lstrQuery .= $lstrOrderBy . ', ';
        }
        $lstrQuery = strings::shorten($lstrQuery, 2);
      } else {
        $lstrQuery .= ' ' . $OrderBy;
      }
      return $lstrQuery;
    }
    return $lstrQuery;
  }

  private function buildColumns($Columns)
  {
    $lstrQuery = '';
    if ($Columns != null) {
      if (is_array($Columns)) {
        $lstrQuery .= ' ';
        if (gettype(array_keys($Columns)[0]) == 'integer') {
          foreach ($Columns as $lstrColumn) {
            $lstrQuery .= $lstrColumn . ', ';
          }
        } else {
          foreach ($Columns as $lstrColumn => $lstrAlias) {
            $lstrQuery .= $lstrColumn . ' as ' . $lstrAlias . ', ';
          }
        }
        $lstrQuery = strings::shorten($lstrQuery, 2);
      } else {
        $lstrQuery .= ' ' . $Columns;
      }
    } else {
      $lstrQuery .= ' *';
    }
    return $lstrQuery;
  }

  private function buildTable($Table)
  {
    $lstrQuery = '';
    if ($Table != null) {
      $lstrQuery .= ' FROM';
      if (is_array($Table)) {
        $lstrQuery .= ' ';
        foreach ($Table as $lmixTable) {
          if (is_array($lmixTable)) {
            $lstrQuery .= $lmixTable[0] . ' AS ' . $lmixTable[1];
          } else {
            $lstrQuery .= $lmixTable . ', ';
          }
        }
        $lstrQuery = strings::shorten($lstrQuery, 2);
      } else {
        $lstrQuery .= ' ' . $Table;
      }
    } else {
      $lstrQuery .= ' *';
    }
    return $lstrQuery;
  }

  private function buildSelectQuery($Table, $Columns, $Where, $OrderBy, $Limit)
  {
    $lstrQuery = 'SELECT';
    $lstrQuery .= $this->buildColumns($Columns);
    $lstrQuery .= $this->buildTable($Table);
    $lstrQuery .= $this->buildWhere($Where);
    $lstrQuery .= $this->buildOrderBy($OrderBy);
    if ($Limit != null) {
      $lstrQuery .= ' LIMIT ' . $Limit;
    }
    return $lstrQuery;
  }

  private function buildQuery($QueryType, $Table, $Columns, $Where, $OrderBy, $Limit)
  {
    $lstrQuery = '';
    switch (strtolower($QueryType)) {
      case 'get':
      case 'select':
      case 'fetch':
        $lstrQuery = $this->buildSelectQuery($Table, $Columns, $Where, $OrderBy, $Limit);
    }
    return $lstrQuery;
  }

  public function first($Table = null, $Columns = null, $Where = null)
  {
    if ($Table == null && $this->Query == null) {
      $Table = $this->Table;
      $Columns = $this->Columns;
      $Where = $this->Where;
    } elseif ($this->Query != null) {
      return $this->execute($this->Query);
    } else {
      return $this->execute($this->buildQuery('get', $Table, $Columns, $Where, null, 1));
    }
  }

  public function count($Table, $Column = null, $ColumnValue = null)
  {
    $lstrSQL = "SELECT COUNT(*) as count FROM " . $Table;
    $lstrSQL = ($Column != null && $ColumnValue != null) ? $lstrSQL . " WHERE " . $Column . " = '" . $ColumnValue . "'" : $lstrSQL;
    return $this->execute($lstrSQL);
  }

  public function get($Table, $Where = null)
  { 
    $lstrQuery = "SELECT * FROM `" .$Table . "`";
    if ($Where != null) {
      $lstrQuery .= $this->whereClause($Where);
    }

    return $this->execute($lstrQuery);
  }

  public function set($Table, $Data)
  {
    $lstrQuery = "INSERT INTO " . $Table . " (";
    $lstrValues = "";
    foreach ($Data as $key => $value) {
      $lstrQuery .= strtolower($key) . ", ";
      $lstrValues .= "'" . $value . "',";
    }
    $lstrQuery = substr($lstrQuery, 0, strlen($lstrQuery) - 2) . ')';
    $lstrQuery .= " VALUES (" . substr($lstrValues, 0, strlen($lstrValues) - 1) . ")";
    return $this->execute($lstrQuery, 'id');
  }

  public function delete($Table, $ID)
  {
    $lstrQuery = "SELECT * FROM `" . $Table . "` WHERE `id` = '" . $SearchData . "'";

    $larrReturnData = $this->execute($lstrQuery);

    $larrReturnData = $larrReturnData[0];

    $this->execute("UPDATE " . $Table . " SET active = 0 WHERE id = " . $larrReturnData['id']);
    unset($larrReturnData['id']); 

    $larrReturnData['status'] = "approvedDeletion";

    $larrReturnData['version'] = intval($larrReturnData['version']) + 1;

    return array('new_id' => $this->set($Table, $larrReturnData));

  }

  public function newVersion($Table, $SearchData, $UpdateData = null, $GetAllBack = false)
  {
   
  }
}
