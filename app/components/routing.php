<?php
// create = Neu und leer
// set = Neu mit Postwerten
// get = Holen mit GET oder POST
// delete = löschen
// update = aktualisieren mit Postwerten
// func = spezielle Funktion aufrufen
//

namespace app\components;

include_once './app/helper/strings.php';

use \app\helper\strings as strings;

/*
/
*/

class router
{
  public function __construct()
  {
    global $app;
  }
  /*
  * @return 
  */
  private function directly()
  {
    global $app;
    if (!isset($_POST['query'])) {
      die($app->t('directly_noQuery'));
    } else {
      return $app->DB->execute($_POST['query']);
    }
  }
  private function create()
  { }

  private function set()
  {
    global $app;
    if (!isset($app->Request['table']) && count($app->Data) == 0) {
      die($app->t('set_noDataAndTablename'));
    }
    if (!isset($app->Request['table']) && count($app->Data) > 0) {
      die($app->t('set_DataButNoTablename'));
    }
    if (isset($app->Request['table']) && count($app->Data) == 0) {
      die($app->t('set_TableButNoData'));
    }

    $lstrQuery = "INSERT INTO " . $app->Request['table'] . ' (';
    $lstrData = '';
    foreach ($app->Data as $key => $value) {
      $lstrQuery .= $key . ', ';
      $lstrData .= "'" . $value . "', ";
    }
    $lstrQuery = strings::shorten($lstrQuery, 2) . ') VALUES (' . strings::shorten($lstrQuery, 2) . ')';
    return $app->DB->execute($lstrQuery, 'id');
  }

  private function config_get()
  { 
    global $app;
    $lstrQuery = "SELECT * FROM `misc_Configuration` WHERE module='".$app->Request['module']."' AND config_group='".$app->Request['group']."'";

    return $app->DB->execute($lstrQuery);
  }
  private function get()
  {
    global $app;
    $lstrQuery = "SELECT * FROM " . $app->Request['table'];
    
    if (isset($app->Request['where'])) {
      $lstrQuery .= " WHERE ";
      foreach ($app->Request['where'] as $key => $value) {
        if (is_array($value)) {
          $lstrQuery .= $key . " ";
          switch ($value[1]) {
            case 'ge':
              $lstrQuery .= " >= '" . $value[0] . "' AND ";
              break;
          }
        } else {
          $lstrQuery .= $key . " = '" . $value . "' AND ";
        }
      }
      $lstrQuery = strings::shorten($lstrQuery, 5);
    }
    if (isset($app->Request['orderby'])) {
      $lstrQuery .= ' ORDER BY';
      if (isset($app->Request['orderby']['columns'])) {
        foreach ($app->Request['orderby']['columns'] as $value) {
          $lstrQuery .= ' ' . $value . ', ';
        }
      } else {
        foreach ($app->Request['orderby'] as $value) {
          $lstrQuery .= ' ' . $value . ', ';
        }
      }
      $lstrQuery = strings::shorten($lstrQuery, 2);
      if (isset($app->Request['orderby']['sortorder'])) {
        $lstrQuery .= ' ' . $app->Request['orderby']['sortorder'];
      }
    }
    return $app->DB->execute($lstrQuery);
  }

  private function update()
  {
    global $app;
    $lstrQuery = "UPDATE " . $app->Request["table"] . " SET ";
    foreach ($app->Data as $key => $value) {
      $lstrQuery .= $key . " = '" . $value . "', ";
    }
    $lstrQuery = substr($lstrQuery, 0, strlen($lstrQuery) - 2) . " WHERE ";
    foreach ($app->Request['where'] as $key => $value) {
      $lstrQuery .= $key . " = '" . $value . "' AND ";
    }
    $lstrQuery = substr($lstrQuery, 0, strlen($lstrQuery) - 5);

    return $app->DB->execute($lstrQuery, 'success');
  }

  private function func()
  {
    global $app;

    $lstrPath = $app->BasePath . 'controller' . DS . $app->Request['controller'] . '.php';
    $larrParams = array();

    if (isset($app->Request['values'])) {
      if (is_object($app->Request['values'])) {
        $larrParams = (array) $app->Request['values'];
      } else {
        $larrParams = $app->Request['values'];
      }
    }
    if (file_exists($lstrPath)) {
      include_once $lstrPath;
      $lstrClass = '\\controller\\' . $app->Request['controller'];
      $reflectionClass = new \ReflectionClass($lstrClass);
      if ($reflectionClass->hasMethod($app->Request['method'])) {
        $reflectionMethod = new \ReflectionMethod($lstrClass, $app->Request['method']);
        if (count($larrParams) > 0) {
          return $reflectionMethod->invokeArgs(new $lstrClass(), $larrParams);
        } else {
          return $reflectionMethod->invoke(new $lstrClass());
        }
      } else {
        return "Unknown method: " . $app->Request['method'];
      }
    }
  }

  public function route()
  {    
    global $app;
    switch ($app->Action) {
      case 'directly':
        return $this->directly();
      case 'create':
        return $this->create();
      case 'set':
        
        return $this->set();
      case 'get':
        return $this->get();
      case 'cget':
        return $this->config_get();
      case 'delete':
        return $app->DB->delete($app->Request['table'], $app->Request['id']);
        break;
      case 'update':
        return $this->update();
      case 'func':
        return $this->func();
    }

    die();
    //------------------


    if (strlen($lstrPath) === 0) {
      die('Es muss ein Pfad angegeben werden');
    }
    if (!strstr($lstrPath, '/')) {
      die('Es wurde kein gültiger Pfad angegeben');
    }

    $larrPath = explode('/', $lstrPath);
    $lstrClassFile = $larrPath[0];
    $lstrMethod = $larrPath[1];
    array_shift($larrPath);
    array_shift($larrPath);

    $larrParams = $larrPath;
    unset($larrPath);

    switch (strtolower($app->RequestMethod)) {
      case 'post':
        $larrParams = $_POST;
        break;
      case 'get':
        if (count($_GET) > 0) {
          array_push($larrParams, $_GET);
        }
        break;
    }

    $lstrPath = $app->BasePath . 'controller' . DS . $lstrClassFile . '.php';

    if (file_exists($lstrPath)) {
      include_once $lstrPath;
      $lstrClass = '\\controller\\' . $lstrClassFile;
      $reflectionClass = new \ReflectionClass($lstrClass);
      if ($reflectionClass->hasMethod($lstrMethod)) {
        $reflectionMethod = new \ReflectionMethod($lstrClass, $lstrMethod);
        if (count($larrParams) > 0) {
          return $reflectionMethod->invokeArgs(new $lstrClass(), $larrParams);
        } else {
          return $reflectionMethod->invoke(new $lstrClass());
        }
      }
    }
  }
}
