<?php
// create = Neu und leer
// set = Neu mit Postwerten
// get = Holen mit GET oder POST
// delete = löschen
// update = aktualisieren mit Postwerten
// func = spezielle Funktion aufrufen
//

namespace app\components;

class translation
{
    private $Language = '';
    private $LanguageStrings = array();
    public function __construct()
    {
        
        global $app;
        if ($this->Language == '') {
            $this->changeLanguage("de_DE");
        }
        
    }
    public function changeLanguage($Language)
    {
        $this->Language = $Language;
    }
    private function loadLanguageFile() {
        $lstrPath = ((strstr($_SERVER['SCRIPT_FILENAME'], 'index.php')) ? str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']) : $_SERVER['SCRIPT_FILENAME']);
        $lstrPath .=   'app'.DS.'translation'.DS.$this->Language.'.json';
        $this->LanguageStrings = json_decode(file_get_contents($lstrPath), true);
    }
    public function translate($String) {
        if (count($this->LanguageStrings) == 0) 
        {
            $this->loadLanguageFile();
        }
        return $this->LanguageStrings[$String];
    }
    public function table($Table) {
        switch ($Table) {
            case 'urgentEntries':
                $Table = 'info_urgentEntries';
                break;
        }
        return $Table;
    }
}
