<?php
namespace app\helper;

include_once './app/helper/crypt.php';
use \app\helper\crypt as crypt;


class actions {
    public function create($Options, $Expires) {
        global $app;
        
        $lstrActionID = crypt::encryptDateTime() . rand(1,100);

        $lstrQuery = "INSERT INTO `misc_actions` (`action_id`, `options`, `expires`) VALUES ('".$lstrActionID."', '".json_encode($Options)."', '".$Expires."')";
        $app->DB->execute($lstrQuery);
        return $lstrActionID;
    }
    public function execute($actionID) {
        global $app;

        $lstrQuery = "SELECT * FROM `misc_actions` WHERE action_id = '" . $actionID . "'";
        $larrResult = $app->DB->execute($lstrQuery);
        $larrOptions = json_decode($larrResult[0]['options']);
        $lstrAction = $larrOptions->type;
        unset($larrOptions->type);

        $_POST['request'] = (array)$larrOptions;
        
        return $lstrAction;

    }
}