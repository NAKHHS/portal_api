<?php
namespace app\helper;

class crypt {
    public function encryptDateTime() {
        $lstrString = crypt::numberToChar(date('y'));
        $lstrString .= crypt::numberToChar(date('m'));
        $lstrString .= crypt::numberToChar(date('d'));
        $lstrString .= crypt::numberToChar(date('H'));
        $lstrString .= crypt::numberToChar(date('i'));
        $lstrString .= crypt::numberToChar(date('s'));

        return $lstrString;
    }
    private function numberToChar($Number) {
        if (strlen($Number) != 2) return false;
        if (intval($Number) > 26) return false;
        switch ($Number) {
            case "0": return '_';
            case "1": return 'a';
            case "2": return 'b';
            case "3": return 'c';
            case "4": return 'd';
            case "5": return 'e';
            case "6": return 'f';
            case "7": return 'g';
            case "8": return 'h';
            case "9": return 'i';
            case "10": return 'j';
            case "11": return 'k';
            case "12": return 'l';
            case "13": return 'm';
            case "14": return 'n';
            case "15": return 'o';
            case "16": return 'p';
            case "17": return 'q';
            case "18": return 'r';
            case "19": return 's';
            case "20": return 't';
            case "21": return 'u';
            case "22": return 'v';
            case "23": return 'w';
            case "24": return 'x';
            case "25": return 'y';
            case "26": return 'z';

        }
    }
}