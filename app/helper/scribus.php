<?php
namespace app\helper;
include_once './app/helper/xml.php';
use \app\helper\xml as XMLProcessing;

class scribus {
  // XPosition immer plus 100
  // YPosition immer plus 20
  // Umrechnung mm in punkte 1mm = 2.83464566929134 Punkte
  // Titelseite
  // -> Linie x:10, y:33, Breite: 23, Rot: 90, Linienbreite 0.75, Farbe: NAK-blau 100%
  // -> Kasten mit Gemeindebrief und Gemeinde
  //    x:10, y:22,233, Breite: 128mm, Höhe: 20mm
  // -> NAK Karitiv: x:10mm, y: 46,644, Breite: 35,499, Höhe: 9
  // -> Monat Jahr: x:10mm, Y: 46,644, Breite: 128mm, Höhe: 9mm
  // -> Kirchenbild: x:10mm, Y: 57,475mm, Breite: 128mm, Höhe: 100mm
  // ---> Interne Positionen der Bilder fehlen
  // -> Wort zum Monat: x: 42,589, y: 162,190, Breite: 95,411, Höhe: 20mm
  // -> NAK-Rahmen: 52,349, y: 190, Breite 66,410 , Höhe 10
  // -> NAK-Logo:,122, y: 184, Breite: 16, Höhe: 16
  // Seitenüberschrift
  // -> Linie: x: 10,375, y:8, Breite: 10, Rotation: 270, Linienbreite: 0,75: Farbe: NAK-blau 100%
  // -> Kasten: x:10,374, y:10, Breite: 127,626, Höhe: 8
  // Seitenunterschrift
  // -> Zahl links: x: 10, y: 196,5, Breite:5, Höhe: 3,5
  // -> Zahl rechts: X: 134,402, y: 196,5, Breite: 5, Höhe 3,5
  // -> Text links: x: 17,05, y: 196,5, Breite: 90, Höhe: 3,5
  // -> Text rechts: x: 41,045, <: 196,5, Breite: 90, Höhe: 3,5
  // Plakat
  // -> Kasten: x: 10,75, y: 22, Breite: 128, Höhe: 174
  // Wort zum Monat
  // -> Text: x: 10, y: 22, Breite: 128, Höhe: 174
  // -> Bild: x: 78,228, y: 22, Breite: 60, Höhe: 52,5
  // -> Nachweis: x: 75,225, y: 74,875, Breite: 79,64, Höhe: 6,747
  private $XML;
  private $cScribusDocument;
  private $cRoot;
  private $cFooter;
  private $cHeader;
  public function __construct() {
    $this->XML = new XMLProcessing('1.0', 'UTF-8');
  }
  public function TitlePage($Congregation, $Month, $Year, $Text, $Position, $Images, $IsDigital) {
    
  }
  private function TitlePageImages() {
  }
  public function Page($Number, $Placing) {
    $this->Root();
    $this->ScribusDocument();
  }
  private function getPageSize($Pagetype) {
    switch (strtolower($Pagetype)) {
    case "a0":
      return array(841, 1189);
    case "a1":
      return array(594, 841);
    case "a2":
      return array(420, 594);
    case "a3":
      return array(297, 420);
    case "a4":
      return array(210, 297);
    case "a5":
      return array(148, 210);
    case "a6":
      return array(105, 148);
    case "a7":
      return array(74, 105);
    case "a8":
      return array(52, 74);
    case "a9":
      return array(37, 52);
    case "a10":
      return array(26, 37);
    }

  }
  public function addPage($Pagenumber = 0, $Pagetype = "a5") {
    $larrPageSize = $this->getPageSize($Pagetype);
    $larrOptions = array(
      "PAGEXPOS" => "100",
      "PAGEYPOS" => "20",
      "PAGEWIDTH" => $this->mmToPoint($larrPageSize[0]),
      "PAGEHEIGHT" => $this->mmToPoint($larrPageSize[1]),
      "BORDERLEFT" => "28.346",
      "BORDERRIGHT" => "28.346",
      "BORDERTOP" => "28.346",
      "BORDERBOTTOM" => "28.346",
      "NUM" => $Pagenumber,
      "NAM" => "",
      "MNAM" => "",
      "Size" => strtoupper($Pagetype),
      "Orientation" => "0",
      "LEFT" => "0",
      "PRESET" => "0",
      "VerticalGuides" => "",
      "HorizontalGuides" => "",
      "AGhorizontalAutoGap" => "0",
      "AGverticalAutoGap" => "0",
      "AGhorizontalAutoCount" => "0",
      "AGverticalAutoCount" => "0",
      "AGhorizontalAutoRefer" => "0",
      "AGverticalAutoRefer" => "0",
      "AGSelection" => "0 0 0 0",
      "pageEffectDuration" => "1",
      "pageViewDuration" => "1",
      "effectType" => "0",
      "Dm" => "0",
      "M" => "0",
      "Di" => "0");
    $this->cScribusDocument->appendChild($this->XML->ElementAttributes('PAGE', $larrOptions));
  }
  public function ScribusDocument() {
    $larrAttributes = include_once '../../Data/ParishLetter/Options/Misc/Document.php';
    $this->cScribusDocument = $this->XML->ElementAttributes('DOCUMENT', $larrAttributes);
  }
  public function PageDefaults() {
    $xmlImportFiles = array(
      "CheckProfile",
      "COLOR",
      "HYPHEN",
      "STYLE",
      "CHARSTYLE",
      "TableStyle",
      "CellStyle",
      "LAYERS",
      "Printer",
      "PDF",
      "DocItemAttributes",
      "TablesOfContents",
      "NotesStyles",
      "NotesFrames",
      "PageSets",
      "Sections",
      "MASTERPAGE",
    );

    foreach ($xmlImportFiles as $lstrFile) {
      $this->cScribusDocument = $this->XML->AddFromFile('./../../Data/ParishLetter/DefaultXML/' . $lstrFile . '.xml', $lstrFile, $this->cScribusDocument);
    }
  }

  private function mmToPoint($mm, $XY = '') {
    $mm = $mm * 2.83464566929134;
    if ($XY != '') {
      $mm = (strtolower($XY) == 'x') ? $mm + 100 : $mm + 20;
    }
    return $mm;
  }

  public function Line($X, $Y, $Rotation, $Length, $Width, $Color) {
    //<PAGEOBJECT XPOS="170.867141732283" YPOS="90.8671417322835" OwnPage="0" ItemID="19262584" PTYPE="5" WIDTH="141.732283464567" HEIGHT="1" FRTYPE="0" CLIPEDIT="0" ROT="90" PWIDTH="2.83464566929134" PCOLOR2="Black" PLINEART="1" path="" ImageRes="2" gXpos="170.867141732283" gYpos="90.8671417322835" gWidth="0" gHeight="0" LAYER="0"/>
    //  <PAGEOBJECT XPOS="128.347456692913" YPOS="48.3474566929134" OwnPage="0" ItemID="158918624" PTYPE="5" WIDTH="56.6929133858268" HEIGHT="1" FRTYPE="0" CLIPEDIT="0" ROT="90" PWIDTH="5.66929133858268" PCOLOR2="White" PLINEART="1" path="" ImageRes="2" gXpos="128.347456692913" gYpos="48.3474566929134" gWidth="0" gHeight="0" LAYER="0"/>
  }
  public function StoryText($Content) {
    $lobjStoryText = $this->XMLProcessing->Element("StoryText");
    $lobjStoryText = $this->XMLProcessing->ElementToExisting("DefaultStyle", $lobjStoryText);
    foreach($Content as $lmixKey => $lmixValue) {
      
    }
  }
  public function addTextElement() {

  }

  public function PageObject($Type, $Options, $Content) {
    switch (strtolower($Type)) {
    case 'textbox':
      $larrDefaultOptions = $this->BoxDefaults();
      $larrDefaultOptions = array_replace($larrDefaultOptions, $Options);      
      $larrOptions = array_merge($larrDefaultOptions, $Options);
      unset($larrDefaultOptions, $Options);
      //$lobjPageObject = $this->XMLProcessing->ElementAttributes('PAGEOBJECT', $larrOptions);
      $lobjStoryText = $this->StoryText($Content);
      break;
    }
  }
  private function BoxDefaults() {
    return array(
      "ItemID" => $this->createItemID(),
      "PTYPE" => "4",
      "FRTYPE" => "0",
      "CLIPEDIT" => "0",
      "PWIDTH" => "1",
      "PLINEART" => "1",
      "LOCALSCX" => "1",
      "LOCALSCY" => "1",
      "LOCALX" => "0",
      "LOCALY" => "0",
      "LOCALROT" => "0",
      "PICART" => "1",
      "SCALETYPE" => "1",
      "RATIO" => "1",
      "COLUMNS" => "1",
      "COLGAP" => "0",
      "AUTOTEXT" => "0",
      "EXTRA" => "0",
      "TEXTRA" => "0",
      "BEXTRA" => "0",
      "REXTRA" => "0",
      "VAlign" => "0",
      "FLOP" => "0",
      "PLTSHOW" => "0",
      "BASEOF" => "0",
      "textPathType" => "0",
      "textPathFlipped" => "0",
      "LAYER" => "0",
      "NEXTITEM" => "-1",
      "BACKITEM" => "-1");
  }
  private function Imagebox() {
    //NEXTITEM="-1" BACKITEM="-1"
  }
  private function createItemID() {
    return mt_rand(100000000, 199999999);
  }
  private function Textbox($Options, $Content) {
    $larrOptions = array(
      "XPOS" => $this->mmToPoint($Options['x'], 'X'),
      "YPOS" => $this->mmToPoint($Options['y'], 'Y'),
      "ItemID" => $this->createItemID(),
      "PTYPE" => "4",
      "WIDTH" => $this->mmToPoint($Options['width']),
      "HEIGHT" => $this->mmToPoint($Options['height']),
      "CLIPEDIT" => "0",
      "LOCALSCX" => "1",
      "LOCALSCY" => "1",
      "SCALETYPE" => "1",
      "COLUMNS" => (isset($Options['columns'])) ? $Options['columns'] : "1",
      "COLGAP" => "8.504",
      "AUTOTEXT" => "0",
      "EXTRA" => "0",
      "TEXTRA" => "0",
      "BEXTRA" => "0",
      "REXTRA" => "0",
      "VAlign" => "0",
      "FLOP" => "0",
      "PLTSHOW" => "0",
      "BASEOF" => "0",
      "textPathType" => "0",
      "textPathFlipped" => "0",
      "path" => "M0 0 L" . number_format($this->mmToPoint($Options['width']), 3) . " 0 L" . number_format($this->mmToPoint($Options['width']), 3) . " " . number_format($this->mmToPoint($Options['height']), 3) . " L0 " . number_format($this->mmToPoint($Options['height']), 3) . " L0 0 Z",
      "copath" => "M0 0 L" . number_format($this->mmToPoint($Options['width']), 3) . " 0 L" . number_format($this->mmToPoint($Options['width']), 3) . " " . number_format($this->mmToPoint($Options['height']), 3) . " L0 " . number_format($this->mmToPoint($Options['height']), 3) . " L0 0 Z",
      "gXpos" => number_format($this->mmToPoint($Options['x'], 'X'), 3),
      "gYpos" => number_format($this->mmToPoint($Options['y'], 'y'), 2),
      "NEXTITEM" => isset($Options['next']) ? $Options['next'] : '-1',
      "BACKITEM" => isset($Options['back']) ? $Options['back'] : '-1');
    $lXMLPageObject = $this->XML->ElementAttributes('PAGEOBJECT', $larrOptions);
  }

  public function PageDefinition() {
    $larrPageValues = include_once '../../Data/ParishLetter/Options/Misc/Page.php';
    $this->cScribusDocument = $this->XML->ElementAttributes('Page', $larrPageValues, $this->cScribusDocument);
  }
  public function Root() {
    $this->cRoot = $this->XML->ElementAttributes("SCRIBUSUTF8NEW", array('Version' => '1.5.4'));
  }
  public function write($PathFile) {
    $this->cRoot->appendChild($this->cScribusDocument);
    $this->XML->AddToBase($this->cRoot);
    $this->XML->save($PathFile);
  }
  public function combinePages() {

  }
}