<?php
namespace app\helper;

class strings {
    public function shorten($String, $Length = 1) {
        return substr($String, 0, strlen($String)-$Length);
    }
}