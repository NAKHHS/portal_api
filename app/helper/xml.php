<?php

namespace app\helper;

use DOMComment;

class xml
{
  private $cxmlDocument;

  public function __construct($Version = null, $Encoding = null)
  {
    if ($Version != null || $Encoding != null) {
      if ($Version != null && $Encoding == null) {
        $this->XMLDocument = new \DOMDocument($Version);
      } elseif ($Version != null & $Encoding != null) {
        $this->XMLDocument = new \DOMDocument($Version, $Encoding);
      }
    } else {
      $this->cxmlDocument = new DOMDocument();
    }
  }

  function save($PathFile)
  {
    $this->XMLDocument->save($PathFile);
  }
  function AddToBase($Element)
  {
    $this->XMLDocument->appendChild($Element);
  }
  function Element($Name, $Value = null)
  {
    if ($Value == null) {
      return $this->XMLDocument->createElement($Name);
    } else {
      return $this->XMLDocument->createElement($Name, $Value);
    }
  }
  function ElementAttributes($Name, $Attributes, $DestElement = null, $Value = null)
  {
    $XMLElement = $this->Element($Name, $Value);
    if ($DestElement != null) {
      $DestElement->appendChild($this->AttributesToExisting($XMLElement, $Attributes));
      return $DestElement;
    } else {
      $XMLElement = $this->AttributesToExisting($XMLElement, $Attributes);
      return $XMLElement;
    }
  }
  function AttributesToExisting($Element, $Attributes)
  {
    foreach ($Attributes as $lstrKey => $lstrValue) {
      $XMLAttribute = $this->XMLDocument->createAttribute($lstrKey);
      $XMLAttribute->value = $lstrValue;
      $Element->appendChild($XMLAttribute);
    }
    return $Element;
  }
  function addFromFileArray($Files, $DestElement, $Tags = null)
  {
    for ($lintCounter = 0; $lintCounter < count($Files); $lintCounter++) {
      $lstrTag = ($Tags != null) ? $Tags[$lintCounter] : $lstrFile;
      $DestElement = $this->AddFromFile($lstrFile, $lstrTag, $DestElement);
    }
  }
  function AddFromFileWithReplace($File, $Tag, $Replaces, $DestElement = null)
  {
    if (file_exists($File)) {
      $lstrData = file_get_contents($File);
      foreach ($Replaces as $ReplaceItemKey => $ReplaceItemValue) {
        $lstrData = str_replace($ReplaceItemKey, $ReplaceItemValue, $lstrData);
      }
      return $this->AddFromFile($lstrData, $Tag, $DestElement);
    }
  }
  function AddFromFile($File, $Tag, $DestElement = null)
  {
    $XMLImportDoc = new \DOMDocument;
    if (substr($File, 0, 1) == '<') {
      $XMLImportDoc->loadXML($File);
    } else {
      $XMLImportDoc->load($File);
    }

    $XMLImportedNodes = $XMLImportDoc->documentElement->childNodes;
    foreach ($XMLImportedNodes as $XMLDomNode) {

      $XMLNewNode = $this->XMLDocument->importNode($XMLDomNode, true);
      $DestElement->appendChild($XMLNewNode);
    }
    return $DestElement;
  }
  function ElementToExisting($Name, $DestElement, $Value = null)
  {
    $DestElement->appendChild($this->Element($Name, $Value));
    return $DestElement;
  }
}
