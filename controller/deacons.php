<?php
namespace controller;

class deacons
{
    private $cstrDataPath = '..' . DS . '..' . DS . 'Data' . DS . 'Diakonplan' . DS;
    public function getabsence($District, $Congregation, $Year, $Month)
    {
        $lstrPath = $this->cstrDataPath . DS . $District . DS . $Congregation . DS . $Year . DS . $Month . '.json';

        if (file_exists($lstrPath)) {
            return json_encode(array("result" => json_decode(file_get_contents($lstrPath))));
        }
    }
    public function createnew($District, $Congregation, $Year, $Month)
    {
        $lstrMonth = 'Januar';
        switch ($Month) {
    case '2':$lstrMonth = 'Februar';
      break;
    case '3':$lstrMonth = 'März';
      break;
    case '4':$lstrMonth = 'April';
      break;
    case '5':$lstrMonth = 'Mai';
      break;
    case '6':$lstrMonth = 'Juni';
      break;
    case '7':$lstrMonth = 'Juli';
      break;
    case '8':$lstrMonth = 'August';
      break;
    case '9':$lstrMonth = 'September';
      break;
    case '10':$lstrMonth = 'Oktober';
      break;
    case '11':$lstrMonth = 'November';
      break;
    case '12':$lstrMonth = 'Dezember';
      break;
    }
        $larrPlan = array();

        $lstrPath = $this->cstrDataPath . DS . $District . DS . $larrParameter[1] . DS . $Congregation . DS . $lstrMonth . '.json';

        $larrData = array();
        if (file_exists($lstrPath)) {
            $larrData = json_decode(file_get_contents($lstrPath));
        }
        $larrAlwaysAbsend = $larrData->dauernd;
        unset($larrData->dauernd);

        $lintNormalDeaconsCount = 2;

        $larrDeacons = json_decode(file_get_contents($this->cstrDataPath . DS . $District . DS . $larrParameter[1] . DS . 'Diakone.json'));
        $larrTemp = array();
        foreach ($larrDeacons as $lstrDeacon) {
            $larrTemp[$lstrDeacon] = array('others' => 0, 'sundays' => 0);
        }
        $larrDeacons = $larrTemp;
        unset($larrTemp);
        foreach ($larrData as $lstrDay => $larrAbsend) {
            $larrEntry = array();
            $lstrDateString = $Congregation . $lstrDay;
            $lstrTime = '193000';
            $lstrServiceDayKey = 'others';
            switch (date('N', strtotime($lstrDateString . 'T000000Z'))) {
      case '7':$lstrTime = '100000';
        $lstrServiceDayKey = 'sundays';
        break;
      case '3':$lstrTime = '193000';
        break;
      default:$lstrTime = '100000';
        $lstrServiceDayKey = 'sundays';
        break;
      }
            $larrEntry['date'] = $lstrDateString . 'T' . $lstrTime . 'Z';
            $larrEntry['remarks'] = '';
            $larrEntry['chief'] = '';
            $larrEntry['absend'] = array_merge($larrAbsend, $larrAlwaysAbsend);

            $larrEntry['who'] = array();

            $larrTempDeacons = array_keys($larrDeacons);
            echo('<hr />');
            echo('<br />Diakone: ');
            $lintServiceWhoManyForDeacon = -1;
            $lstrServiceDeaconName = '';
            do {
                $larrTempDeacons = array_diff($larrTempDeacons, $larrEntry['absend']);
                if (count($larrTempDeacons) > 0) {
                    $lintDeaconTempIndex = array_rand($larrTempDeacons);

                    echo('<br />Der Index des Diakons ist: ' . $lintDeaconTempIndex);
                    $lstrDeacon = $larrTempDeacons[$lintDeaconTempIndex];

                    echo('<br />Der Name des Diakons ist: ' . $lstrDeacon);
                    if ($lintServiceWhoManyForDeacon == -1 && $lstrServiceDeaconName == '') {
                        $lintServiceWhoManyForDeacon = $larrDeacons[$lstrDeacon][$lstrServiceDayKey];
                        $lintServiceDeaconName = $lstrDeacon;
                    } elseif ($larrDeacons[$lstrDeacon][$lstrServiceDayKey] < $lintServiceWhoManyForDeacon) {
                        $lintServiceWhoManyForDeacon = $larrDeacons[$lstrDeacon][$lstrServiceDayKey];
                        $lintServiceDeaconName = $lstrDeacon;
                    }

                    if ($larrDeacons[$lstrDeacon][$lstrServiceDayKey] == 0) {
                        $larrTempDeacons = array();
                    }
                    array_splice($larrTempDeacons, $lintDeaconTempIndex, 1);

                    if (count($larrTempDeacons) <= 0) {
                        array_push($larrEntry['who'], $lstrDeacon);
                        $larrDeacons[$lstrDeacon][$lstrServiceDayKey]++;
                        $larrTempDeacons = array_keys($larrDeacons);
                        $larrTempDeacons = array_diff($larrTempDeacons, $larrEntry['who']);
                        $lstrServiceDeaconName = '';
                        $lintServiceWhoManyForDeacon = -1;
                    }
                }
            } while (count($larrEntry['who']) < $lintNormalDeaconsCount);

            array_push($larrPlan, $larrEntry);
        }
        die();
        unset($larrAlwaysAbsend);

        for ($lintEntryCounter = 0; $lintEntryCounter < count($larrPlan); $lintEntryCounter++) {
            $lintHowMany = -1;
            $lstrWho = '';
            do {
                if (!in_array($lstrDeacon, $larrEntry['absend'])) {
                    $lstrKey = ($lboolIsSunday) ? 'sundays' : 'others';
                    if ($larrDeacons[$lstrDeacon][$lstrKey] == 0) {
                        array_push($larrEntry['who'], $lstrDeacon);
                        $larrDeacons[$lstrDeacon][$lstrKey]++;
                        array_splice($larrTempDeacons, $lintDeaconTempIndex, 1);
                    } else {
                        if ($larrDeacons[$lstrDeacon][$lstrKey] < $lintHowMany) {
                            $lintHowMany = $larrDeacons[$lstrDeacon][$lstrKey];
                            $lstrWho = $lstrDeacon;
                        } elseif ($lintHowMany == -1 && $lstrWho == '') {
                            $lintHowMany = $larrDeacons[$lstrDeacon][$lstrKey];
                            $lstrWho = $lstrDeacon;
                            array_splice($larrTempDeacons, $lintDeaconTempIndex, 1);
                        } else {
                            array_splice($larrTempDeacons, $lintDeaconTempIndex, 1);
                        }
                    }
                    if (count($larrTempDeacons) == 0) {
                        array_push($larrEntry['who'], $lstrWho);
                        $larrDeacons[$lstrWho][$lstrKey]++;

                        if (count($larrEntry['who']) < 2) {
                            $larrTempDeacons = array_diff(array_keys($larrDeacons), $larrEntry['who']);
                            $lintHowMany = -1;
                            $lstrWho = '';
                        }
                    }

                    if (count($larrEntry['who']) >= 2) {
                        if (count($larrEntry['who']) == 2) {
                            break;
                        } elseif (count($larrEntry['who']) > 2) {
                            $lmixDeacon = mt_rand(0, count($larrEntry['who']) - 1);
                            $lstrDeacon = $larrEntry['who'][$lmixDeacon];
                            array_splice($larrEntry['who'], $lmixDeacon, 1);
                            $larrDeacons[$lstrDeacon][$lstrKey]--;
                        }
                    }
                } else {
                    array_splice($larrTempDeacons, $lintDeaconTempIndex, 1);
                }
            } while (count($larrTempDeacons) > 0);
            if (count($larrEntry['who']) < 2) {
                echo('<br /><b>Zu wenige Diakone</b>' . $larrEntry['date']);
            } elseif (count($larrEntry['who']) > 2) {
                echo('<br /><b>Zu viele Diakone</b>' . $larrEntry['date']);
            }
            $larrPlan[$lintEntryCounter] = $larrEntry;
        }

        die();

        return json_encode(array("result" => $larrPlan));
    }
}
