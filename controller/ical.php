<?php

namespace controller;

include_once  './app/components/database.php';

use \app\components\database as db;

class ical
{
  private $cstrDataPath = '..' . DS . '..' . DS . 'Data' . DS . 'Terminplanung' . DS . 'Hamburg-Süd' . DS;
  private $DB;
  private $carrAppointments = array();
  private $carrRemoteAppointments = array();
  private $carrRemoteWithoutUID = array();
  private $carrUpdateAppointments = array();
  private $carrManuallyCheck = array();
  private $carrActions = array();

  public function __construct()
  {
    $this->DB = new db();
  }

  public function get($Filter = null)
  {
    $lstrQuery = "SELECT * FROM sched_Appointment ";

    if (gettype($Filter) == 'string' && $Filter != 'json') {
      $lstrQuery .= "WHERE CATEGORIES = '" . $Filter . "'";
    } elseif (gettype($Filter) == 'array') {
      $lstrQuery .= "WHERE ";
      foreach ($Filter as $lstrFilterEntry) {
        $lstrQuery .= "CATEGORIES = '" . $lstrFilterEntry . "' OR ";
      }
      $lstrQuery = \substr($lstrQuery, 0, strlen($lstrQuery) - 4);
    }

    $lstrQuery .= " ORDER BY DTSTART, CATEGORIES";
    return $this->DB->execute($lstrQuery);
  }

  public function read()
  {

    if ($this->getFiles()) {
      //$this->DB->empty("sched_Appointment");
      $this->parse();
    }
  }

  private function parse()
  {
    $this->carrRemoteAppointments = $this->DB->execute("SELECT * FROM `sched_Appointments` WHERE `active` = 1 AND `dtstart` >= NOW() ORDER BY `dtstart` ASC");
    $larrFiles = array(
      "Bezirk" => array("File" => "Bezirk%20HH-Sued.ics", "Bezirk" => true),
      "Buxtehude" => array("File" => "GBux.ics", "Category" => "Gemeinde Bux"),
      "Harburg" => array("File" => "GHar.ics", "Category" => "Gemeinde Har"),
      "Neugraben" => array("File" => "GNeu.ics", "Category" => "Gemeinde Neu"),
      "Sinstorf" => array("File" => "GSin.ics", "Category" => "Gemeinde Sin"),
      "Nordheide" => array("File" => "GNor.ics", "Category" => "Gemeinde Nor"),
      "Rotenburg" => array("File" => "GRoW.ics", "Category" => "Gemeinde RoW"),
      "Winsen" => array("File" => "GWin.ics", "Category" => "Gemeinde Win"),
    );
    $this->process($larrFiles);
  }
  public function process($Files)
  {
    $this->carrActions['newly_inserted'] = 0;
    $this->carrActions['updated'] = 0;
    $this->carrActions['error'] = 0;
    foreach ($Files as $larrFile) {
      $this->iCalDecoder($larrFile);
    }

    if (!is_null($this->carrRemoteAppointments)) {
      if (count($this->carrRemoteAppointments) > 0) {
        for ($lintRemoteAppointmentsCounter = 0; $lintRemoteAppointmentsCounter < count($this->carrRemoteAppointments); $lintRemoteAppointmentsCounter++) {
          if ($this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['uid'] == "") {
            array_push($this->carrRemoteWithoutUID, $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]);
            $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['DELETE'] = true;
          } else {
            $this->compareAppointments($lintRemoteAppointmentsCounter);
          }
        }
      }
    }

    $this->carrRemoteAppointments = $this->deleteDatasetsInArray($this->carrRemoteAppointments);
    $this->carrAppointments = $this->deleteDatasetsInArray($this->carrAppointments);

    if (count($this->carrRemoteWithoutUID) > 0) {
      for ($lintCounter = 0; $lintCounter < count($this->carrRemoteWithoutUID); $lintCounter++) {
        for ($lintiCalCounter = 0; $lintiCalCounter < count($this->carrAppointments); $lintiCalCounter++) {
          if (!isset($this->carrAppointments[$lintiCalCounter]['DELETE'])) {
            $lintDifferences = $this->findDifferences($this->carrRemoteWithoutUID[$lintCounter], $this->carrAppointments[$lintiCalCounter]);

            if ($lintDifferences >= 0 && $lintDifferences < 4) {
              $this->carrAppointments[$lintiCalCounter]['id'] = $this->carrRemoteWithoutUID[$lintCounter]['id'];
              array_push($this->carrUpdateAppointments, $this->carrAppointments[$lintiCalCounter]);
              $this->carrAppointments[$lintiCalCounter]['DELETE'] = true;
              $this->carrRemoteWithoutUID[$lintCounter]['DELETE'] = true;
              break 1;
            }
            if ($lintDifferences >= 4 && $lintDifferences < 6) {
              array_push($this->carrManuallyCheck, $this->carrAppointments[$lintiCalCounter]);
              $this->carrAppointments[$lintiCalCounter]['DELETE'] = true;
              $this->carrRemoteWithoutUID[$lintCounter]['DELETE'] = true;
              break 1;
            }
          }
        }
      }
    }

    $this->carrRemoteWithoutUID = $this->deleteDatasetsInArray($this->carrRemoteAppointments);
    $this->carrAppointments = $this->deleteDatasetsInArray($this->carrAppointments);

    if (count($this->carrAppointments) > 0) {
      foreach ($this->carrAppointments as $larrAppointment) {
        $this->DatabaseOperation($larrAppointment);
      }
    }
    if (count($this->carrUpdateAppointments) > 0) {
      foreach ($this->carrUpdateAppointments as $larrAppointment) {
        $this->DatabaseOperation($larrAppointment, 'update');
      }
    }
  }

  private function deleteDatasetsInArray($larrArray)
  {
    if (!is_null($larrArray) && count($larrArray) > 0) {
      $larrTemp = array();
      for ($lintCounter = 0; $lintCounter < count($larrArray); $lintCounter++) {
        if (!isset($larrArray[$lintCounter]['DELETE'])) {
          array_push($larrTemp, $larrArray[$lintCounter]);
        }
      }
      return $larrTemp;
    } else {
      return null;
    }
  }

  private function DatabaseOperation($Appointment, $Type = 'insert')
  {
    global $app;
    $lstrVersion = 1;
    if (strtolower($Type) == 'update') {
      $lstrVersion = $app->DB->newVersion("sched_Appointments", $Appointment['id']);
    }

    $lstrSQL = "";

    $lstrSQL .= "INSERT INTO sched_Appointments (";
    $lstrSQL .= (isset($Appointment['UID'])) ? "uid" : "";
    $lstrSQL .= (isset($Appointment['LOCATION'])) ? ", location" : "";
    $lstrSQL .= (isset($Appointment['SUMMARY'])) ? ", summary" : "";
    $lstrSQL .= (isset($Appointment['DESCRIPTION'])) ? ", description" : "";
    $lstrSQL .= (isset($Appointment['CLASS'])) ? ", class" : "";
    $lstrSQL .= (isset($Appointment['DTSTART'])) ? ", dtstart" : "";
    $lstrSQL .= (isset($Appointment['DTEND'])) ? ", dtend" : "";
    $lstrSQL .= (isset($Appointment['DTSTAMP'])) ? ", dtstamp" : "";
    $lstrSQL .= (isset($Appointment['CATEGORIES'])) ? ", categories" : "";
    $lstrSQL .= (isset($Appointment['MODIFIED'])) ? ", last_modified" : "";
    $lstrSQL .= ', version';
    $lstrSQL .= ") VALUES (";

    $lstrSQL .= (isset($Appointment['UID'])) ? "'" . $Appointment['UID'] . "'" : "";
    $lstrSQL .= (isset($Appointment['LOCATION'])) ? ", '" . $Appointment['LOCATION'] . "'"  : "";
    $lstrSQL .= (isset($Appointment['SUMMARY'])) ? ", '" . $Appointment['SUMMARY'] . "'"  : "";
    $lstrSQL .= (isset($Appointment['DESCRIPTION'])) ? ", '" . $Appointment['DESCRIPTION'] . "'"  : "";
    $lstrSQL .= (isset($Appointment['CLASS'])) ? ", '" . $Appointment['CLASS'] . "'"  : "";
    $lstrSQL .= (isset($Appointment['DTSTART'])) ? ", '" . date('Y-m-d H:i:s', strtotime($Appointment['DTSTART'])) . "'"  : "";
    $lstrSQL .= (isset($Appointment['DTEND'])) ? ", '" . date('Y-m-d H:i:s', strtotime($Appointment['DTEND'])) . "'"  : "";
    $lstrSQL .= (isset($Appointment['DTSTAMP'])) ? ", '" . date('Y-m-d H:i:s', strtotime($Appointment['DTSTAMP'])) . "'"  : "";
    $lstrSQL .= (isset($Appointment['CATEGORIES'])) ? ", '" . $Appointment['CATEGORIES'] . "'"  : "";
    $lstrSQL .= (isset($Appointment['MODIFIED'])) ? ", '" . date('Y-m-d H:i:s', strtotime($Appointment['MODIFIED'])) . "'" : "";
    $lstrSQL .= ", '" . $lstrVersion . "'";
    $lstrSQL .= ");";
    if ($lstrSQL != "") {
      if (mb_detect_encoding($lstrSQL) != 'UTF-8') {
        $lstrSQL = utf8_encode($lstrSQL);
      }
      if (strtolower($Type) == 'update') {
        $this->carrActions['updated']++;
      } else {
        $this->carrActions['newly_inserted']++;
      }

      $this->DB->execute($lstrSQL);
    }
  }

  public function iCalDecoder($FileData)
  {
    //$File = 'http://www.menne.biz/NAK/' . $FileData['File'];
    $File = 'http://e.hh-sued.de/testdaten/' . $FileData['File'];
    $lstrICalData = file_get_contents($File);

    $lstrICalData = str_replace("\xEF\xBB\xBF", '', $lstrICalData);

    $bom = pack('H*', 'EFBBBF');
    $lstrICalData = preg_replace("/^$bom/", '', $lstrICalData);

    preg_match_all('/(BEGIN:VEVENT.*?END:VEVENT)/si', $lstrICalData, $larrResult, PREG_PATTERN_ORDER);
    for ($lintCounter = 0; $lintCounter < count($larrResult[0]); $lintCounter++) {
      $lstrDataset = explode("rn", $larrResult[0][$lintCounter]);
      $lstrDataset = $lstrDataset[0];
      $lstrDataset = ltrim(str_replace('BEGIN:VEVENT', '', $lstrDataset));
      $lstrDataset = rtrim(str_replace('END:VEVENT', '', $lstrDataset));
      $lstrDataset = rtrim(str_replace('CLASS:PUBLIC', '', $lstrDataset));

      preg_match_all('/[A-Z]*:/', $lstrDataset, $larrKeys);

      $larrValues = preg_split('/[A-Z]*:/', $lstrDataset);
      if ($larrValues[0] == '') {
        array_shift($larrValues);
      }

      $larrKeys = $larrKeys[0];

      $larrTemp = array();

      for ($lintKeysCounter = 0; $lintKeysCounter < count($larrKeys); $lintKeysCounter++) {
        $lstrKey = $larrKeys[$lintKeysCounter];
        $lstrValue = $larrValues[$lintKeysCounter];
        if (substr($lstrKey, strlen($lstrKey) - 1) == ':') {
          $lstrKey = substr($lstrKey, 0, strlen($lstrKey) - 1);
        }
        $larrTemp[$lstrKey] = rtrim($lstrValue);
        $larrTemp[$lstrKey] = ltrim($larrTemp[$lstrKey]);
      }

      if (isset($larrTemp['CATEGORIES'])) {
        if (isset($FileData['Bezirk']) || strpos($larrTemp['CATEGORIES'], $FileData['Category']) > -1) {
          if (array_key_exists('DTSTART', $larrTemp)) {
            if (strtotime($larrTemp['DTSTART']) >= time()) {
              $larrTemp['CATEGORIES'] = (strstr($larrTemp['CATEGORIES'], "\r\nLAST-")) ? str_replace("\r\nLAST-", '', $larrTemp['CATEGORIES']) : $larrTemp['CATEGORIES'];

              $this->checkAppointmentExists($larrTemp);
            }
          }
        }
      }
    }
  }

  private function checkAppointmentExists($Appointment)
  {
    $lboolExists = false;
    foreach ($this->carrAppointments as $larrAppointment) {
      if ($larrAppointment['UID'] == $Appointment['UID']) {
        $lboolExists = true;
        break;
      }
    }
    if (!$lboolExists) {
      array_push($this->carrAppointments, $Appointment);
    }
  }

  private function compareAppointments($lintRemoteAppointmentsCounter)
  {
    if (!isset($this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['DELETE'])) {
      for ($lintAppointmentCounter = 0; $lintAppointmentCounter < count($this->carrAppointments); $lintAppointmentCounter++) {
        if (!isset($this->carrAppointments[$lintAppointmentCounter]['DELETE'])) {
          if ($this->carrAppointments[$lintAppointmentCounter]['UID'] == $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['uid']) {
            $lintDifferences = $this->findDifferences($this->carrRemoteAppointments[$lintRemoteAppointmentsCounter], $this->carrAppointments[$lintAppointmentCounter]);

            if ($lintDifferences == 0) {
              $this->carrAppointments[$lintAppointmentCounter]['DELETE'] = true;
              $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['DELETE'] = true;
            }
            if ($lintDifferences > 0 && $lintDifferences < 4) {
              $this->carrAppointments[$lintAppointmentCounter]['id'] = $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['id'];
              array_push($this->carrUpdateAppointments, $this->carrAppointments[$lintAppointmentCounter]);
              $this->carrAppointments[$lintAppointmentCounter]['DELETE'] = true;
              $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['DELETE'] = true;
            }
            if ($lintDifferences >= 4 && $lintDifferences < 6) {
              $this->carrAppointments[$lintAppointmentCounter]['id'] = $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['id'];
              array_push($this->carrManuallyCheck, $this->carrAppointments[$lintAppointmentCounter]);
              $this->carrAppointments[$lintAppointmentCounter]['DELETE'] = true;
              $this->carrRemoteAppointments[$lintRemoteAppointmentsCounter]['DELETE'] = true;
            }

            break;
          }
        }
      }
    }
  }

  private function findDifferences($RemoteAppointment, $IcalAppointment)
  {
    $lintDifferences = 0;

    if ($RemoteAppointment['location'] != $IcalAppointment['LOCATION']) $lintDifferences++;
    if ($RemoteAppointment['summary'] != $IcalAppointment['SUMMARY']) $lintDifferences++;
    if ($RemoteAppointment['description'] == "" && $IcalAppointment['DESCRIPTION'] == "") $lintDifferences++;
    if ($RemoteAppointment['description'] != $IcalAppointment['DESCRIPTION']) $lintDifferences++;
    if ($RemoteAppointment['dtstart'] != date('Y-m-d H:i:s', strtotime($IcalAppointment['DTSTART']))) $lintDifferences++;
    if ($RemoteAppointment['dtend'] != date('Y-m-d H:i:s', strtotime($IcalAppointment['DTEND']))) $lintDifferences++;
    if ($RemoteAppointment['categories'] != $IcalAppointment['CATEGORIES']) $lintDifferences++;

    if ($lintDifferences == 1 && $RemoteAppointment['description'] == "" && $IcalAppointment['DESCRIPTION'] == "") {
      $lintDifferences--;
    }

    return $lintDifferences;
  }
  /*private function checkDataset($Dataset)
  {
    if (isset($Dataset['UID'])) {
      return (intval($this->DB->count('sched_Appointments', 'uid', $Dataset['UID'])[0]['count']) > 0) ? true : false;
    }
    return false;
  }*/

  private function getFiles()
  {
    return true;

    //--------------------------------------

    $lboolParse = false;
    //$lstrHeaders = get_headers('http://www.menne.biz/NAK/Bezirk%20HH-Sued.ics', 1);
    $lstrHeaders = get_headers('http://e.hh-sued.de/testdaten/Bezirk%20HH-Sued.ics', 1);
    //$h = get_headers('http://www.menne.biz/NAK/GBux.ics', 1);

    $ldtDateTime = null;
    if (strstr($lstrHeaders[0], '200') != false) {
      $lstrLastSync = '';
      $ldtDateTime = new \DateTime($lstrHeaders['Last-Modified']);
      $lstrDateTime = $ldtDateTime->format('Y-m-d H:i:s');
      if (file_exists($this->cstrDataPath . 'lastSync')) {
        $ltsLastSync = strtotime(file_get_contents($this->cstrDataPath . 'lastSync'));
        if (strtotime($lstrDateTime) >= $ltsLastSync) {
          $lboolParse = true;
        }
      } else {
        $lboolParse = true;
      }
    }
    return $lboolParse;
  }
}
