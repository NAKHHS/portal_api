<?php
namespace controller;

class infocenter
{
    public function getUrgent()
    {
        global $app;
    
        $larrResult = $app->DB->Query("SELECT * FROM `info_urgentEntries` WHERE `date_to` >= " . date('Y-m-d') . " AND `active` = 1")->execute();
        
        if ($larrResult) {
            $lintCounter = 0;
            foreach($larrResult as $larrEntry) {
                $dafj = $larrEntry['date_from'] . 'T22:00:00.000Z';
                $larrResult[$lintCounter]['date_range'] = array();
                array_push($larrResult[$lintCounter]['date_range'],$larrEntry['date_from'] . 'T22:00:00.000Z');
                array_push($larrResult[$lintCounter]['date_range'],$larrEntry['date_to'] . 'T22:00:00.000Z');
                unset($larrResult[$lintCounter]['date_from']);
                unset($larrResult[$lintCounter]['date_to']);
                $lintCounter++;
            }
            return $larrResult;
        } else {
            return false;
        }
    }
    public function DeleteUrgentEntry($ID) {
        global $app;
        $app->DB->delete('info_urgentEntries', $ID);
    }
    public function SaveUrgentEntry() {
        global $app;

        $larrData = $_POST['data'];
        unset($_POST);
        $larrData['date_from'] = explode ("T",$larrData['date_range'][0])[0];
        $larrData['date_to'] = explode ("T",$larrData['date_range'][1])[0];
        unset($larrData['date_range']);

        $app->DB->set('info_urgentEntries', $larrData);
    }
}
