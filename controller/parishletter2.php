<?php
namespace controller;

class parishletter
{
    private $cstrDataPath = '..'.DS.'..'.DS.'Data'.DS.'Gemeindebrief'.DS;
    function units() {
        $larrFolders = array_diff(scandir(parishletter::$cstrDataPath.'Einheiten'.DS), array('.', '..'));
        return json_encode(array("result"=> $larrFolders));
    }
    function years() {
        list($lstrMethod, $larrParameter) = func_get_args();
        $larrFolders = array_diff(scandir(parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Jahre'.DS), array('.', '..'));
        return json_encode(array("result"=> $larrFolders));
    }
    function months() {
        
        list($lstrMethod, $larrParameter) = func_get_args();
        $larrFolders = array_diff(scandir(parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Jahre'.DS.$larrParameter[1]), array('.', '..'));
        return json_encode(array("result"=> $larrFolders));
    }
    function content() {
        
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Jahre'.DS.$larrParameter[1].DS.$larrParameter[2].DS.'Inhalt'.DS.'__Inhalt.json';

        return json_encode(array("result"=> json_decode(file_get_contents($lstrPath))));
    }
    function element() {
        
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Jahre'.DS.$larrParameter[1].DS.$larrParameter[2].DS.'Inhalt'.DS.$larrParameter[3].'.json';

        return json_encode(array("result"=> json_decode(file_get_contents($lstrPath))));
    }
    function text() {
        
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Jahre'.DS.$larrParameter[1].DS.$larrParameter[2].DS.'Texte'.DS.$larrParameter[3];
        
        return json_encode(array("result"=> file_get_contents($lstrPath)));
    }
    function congregations() {
        
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Konfiguration'.DS.'Gemeinden.json';
        
        return json_encode(array("result"=> json_decode(file_get_contents($lstrPath))));

    }
    function options() {
        
        list($lstrMethod, $larrParameter) = func_get_args();
    }
    function congregationinformation() {
        
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter[0].DS.'Konfiguration'.DS.'Letzte_Seite'.DS.$larrParameter[1].'.json';
        
        return json_encode(array("result"=> json_decode(file_get_contents($lstrPath))));
    }
    function setinformation() {
        
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrPath = parishletter::$cstrDataPath.'Einheiten'.DS.$larrParameter["Bereich"].DS.'Konfiguration'.DS.'Letzte_Seite'.DS.$larrParameter["Gemeinde"].'.json';
        
        $larrData = json_decode(file_get_contents($lstrPath),true);
        
        switch(count($larrParameter['Pfad'])) 
        {
            case 1:
                $larrData[$larrParameter['Pfad'][0]] = $larrParameter['Wert'];
                break;
            case 2:
                $larrData[$larrParameter['Pfad'][0]][$larrParameter['Pfad'][1]] = $larrParameter['Wert'];
            break;
            case 3:
            $larrData[$larrParameter['Pfad'][0]][$larrParameter['Pfad'][1]][$larrParameter['Pfad'][2]] = $larrParameter['Wert'];
            break;
        }
        file_put_contents($lstrPath,json_encode($larrData));
    }
    // -----------------------------------------------------------------------------------------------------
    /*function groups()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrQuery        = "SELECT pl_Groups.Title, pl_Groups.ID FROM pl_ParishLetter, pl_Groups WHERE pl_ParishLetter.Group_ID = pl_Groups.ID";
        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        foreach ($larrResult as $lmixEntry) {
            array_push($larrResultValues, $lmixEntry);
        }
        return json_encode(array(
            "result" => $larrResultValues
        ));
    }
    function years()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrQuery = "SELECT DISTINCT(Year),Year  FROM `pl_ParishLetter` WHERE Group_ID = '" . $larrParameter[0] . "'";
        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        if (count($larrResult) > 0) {
            foreach ($larrResult as $lmixEntry) {
                array_push($larrResultValues, $lmixEntry['Year']);
            }
            return json_encode(array(
                "result" => $larrResultValues
            ));
        } else {
            return json_encode(array(
                "result" => false
            ));
        }
    }
    function months()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        $lstrQuery = "SELECT ID, Month FROM `pl_ParishLetter` WHERE Group_ID = " . $larrParameter[0] . " AND Year = '" . $larrParameter[1] . "'";
        
        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        foreach ($larrResult as $lmixEntry) {
            array_push($larrResultValues, $lmixEntry);
        }
        return json_encode(array(
            "result" => $larrResultValues
        ));
    }
    function parishLetter()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        switch ($lstrMethod) {
            case "GET":
                $lstrQuery = "SELECT * FROM `pl_ParishLetter` WHERE Year = '" . $larrParameter[0] . "' AND Month = '" . $larrParameter[1] . "' and District = '" . $larrParameter[2] . "'";
                break;
        }
    }
    function getGroupMainID() {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrQuery = "SELECT Main_Location FROM `pl_Groups` WHERE ID = " . $larrParameter[0];
        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        foreach ($larrResult as $lmixEntry) {
            array_push($larrResultValues, $lmixEntry);
        }
        return json_encode(array(
            "result" => $larrResultValues
        ));

    }
    function elements()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrQuery = "SELECT ID, Title FROM `pl_Elements` WHERE Page = '".$larrParameter[0]."' ORDER BY Position";

        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        if (count($larrResult) > 0) {
            foreach ($larrResult as $lmixEntry) {
                array_push($larrResultValues, $lmixEntry);
            }
            return json_encode(array(
                "result" => $larrResultValues
            ));
        } else {
            return json_encode(array(
                "result" => false
            ));
        }
    }
    function pages()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrQuery = "SELECT ID, Title FROM `pl_Pages` WHERE ParishLetter = '".$larrParameter[0]."' AND Congregation = '".$larrParameter[1]."' ORDER BY Position";

        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        if (count($larrResult) > 0) {
            foreach ($larrResult as $lmixEntry) {
                array_push($larrResultValues, $lmixEntry);
            }
            return json_encode(array(
                "result" => $larrResultValues
            ));
        } else {
            return json_encode(array(
                "result" => false
            ));
        }
    }
    function congregation () {
        
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrQuery = "SELECT Data FROM pl_Location_Config WHERE Location = " . $larrParameter[0];
        
        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        if (count($larrResult) > 0) {
            return json_encode(array(
                "result" => $larrResult[0]['Data']
            ));
        } else {
            die('Dirt');
            return json_encode(array(
                "result" => false
            ));
        }
    }
    function congregations()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();

        $lstrQuery = "SELECT chu_Locations.Name, pl_Groups_Members.Location FROM pl_Groups_Members, chu_Locations WHERE pl_Groups_Members.Group_ID = ".$larrParameter[0]." AND pl_Groups_Members.Location = chu_Locations.ID ORDER BY chu_Locations.Name";

        $larrResult       = DSb->get($lstrQuery);
        $larrResultValues = array();
        if (count($larrResult) > 0) {
            foreach ($larrResult as $lmixEntry) {
                array_push($larrResultValues, $lmixEntry);
            }
            return json_encode(array(
                "result" => $larrResultValues
            ));
        } else {
            return json_encode(array(
                "result" => false
            ));
        }
    }
    function getChildren()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        $lstrTable = $larrParameter[0];
    }
    function titlePageBibleWord()
    {
        global DSb;
        list($lstrMethod, $larrParameter) = func_get_args();
        
        switch ($lstrMethod) {
            case "GET":
                $lstrQuery = "SELECT Data FROM `pl_ParishLetter` WHERE ID = " . $larrParameter[0];
                if (DSb->count($lstrQuery) == 1) {
                    $larrResult = DSb->get($lstrQuery);
                    return json_encode(array(
                        "result" => $larrResult[0]['Data']
                    ));
                } else {
                    return json_encode(array(
                        "result" => "false"
                    ));
                }
                break;
            case "POST":
                break;
            case "PUT":
                break;
        }
        
        
    } */
}