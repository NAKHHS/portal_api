<?php

namespace controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include_once './app/helper/strings.php';
include_once './app/helper/mail.php';
include_once './app/helper/actions.php';

use \app\helper\strings as strings;
use \app\helper\mail as mail;
use \app\helper\actions as actions;

class scheduling
{
  function Summary()
  {
    $lstrSQL = "SELECT * FROM `sched_Appointments` WHERE `status` IN ('approvedDeletion', 'approvedCreation', 'approvedEdit')";
  }
  function fetchAppointments()
  {
    $lstrPath = $this->cstrDataPath . 'iCalData.json';

    return json_encode(array("result" => json_decode(file_get_contents($lstrPath))));
  }
  function deleteFromMail($ID)
  {
    global $app;
    $lstrQuery = "DELETE FROM `sched_Appointments` WHERE id = " . $ID;
    $app->DB->execute($lstrQuery);
  }
  function editAppointment($Location, $Summary, $DTStart, $Categories, $ChangerName)
  { }
  function deleteAppointment($Location, $Summary, $DTStart, $Categories, $ChangerName, $ChangerEmail, $ID)
  {
      global $app;

      $lmixSearchData = '';
      if ($ID != '') {
          $lmixSearchData = $ID;
      } else {
          $lmixSearchData = array(
        'location' => $Location,
        'summary' => $Summary,
        'dtstart' => $DTStart,
        'categories' => $Categories
      );
      }

      $larrReturnData = $app->DB->delete("sched_Appointments", $lmixSearchData, array('email' => $ChangerEmail, 'name' => $ChangerName));

      $ldtExpireDate = date('Y-m-d H:i:s');

      if ($larrReturnData['new_id'] > 0) {
          $lstrActionIDNotApproved = actions::create(
          array(
          'type' => 'func',
          'controller' => 'scheduling',
          'method' => 'deletedAppointmentNotApproved',
          'values' => array('id' => $larrReturnData['new_id'])
        ),
          $ldtExpireDate
      );
          $lstrActionApproved = actions::create(
          array(
          'type' => 'func',
          'controller' => 'scheduling',
          'method' => 'deletedAppointmentApproved',
          'values' => array('id' => $larrReturnData['new_id'])
        ),
          $ldtExpireDate
      );
      }
      //Mail

      $lstrMailText = "Hallo " . $ChangerName . "<br /><br />";
      if ($larrReturnData['oldData']['user_email'] == $ChangerEmail) {
          $lstrMailText .= "Mit Deiner E-Mail-Adresse '" . $ChangerEmail . "' wurde ein Termin gelöscht. Bitte prüfe, ob Du diese Aktion durchgeführt hast. Solltest Du nichts machen, so wird die Änderung um " . $ldtExpireDate . " abgelehnt!";
      } elseif ($larrReturnData['oldData']['user_email'] != $ChangerEmail) {
        $lstrMailText .= "Ein Benutzer mit der E-Mail-Adresse '" . $ChangerEmail . "' hat einen von Dir zuletzt bearbeiteten Termin gelöscht. Solltest Du mit dieser Änderung einverstanden sein, so bestätige bitte den Termin unten.";
      } elseif ($larrReturnData['oldData']['user_email'] == 'import_job') {
        $lstrMailText .= "Mit Deiner E-Mail-Adresse '" . $ChangerEmail . "' wurde ein Termin gelöscht, welcher bisher noch keinen Ersteller hatte. Bitte prüfe, ob das Löschen richtig war.";
      }
      
    $lstrMailText .= $this->MailText($Summary, $DTStart, '', '', $Categories, $Location);

    $lstrMailText .= '<a href="http://localhost/Portal/API/V3/index.php?action=' . $lstrActionIDNotApproved . '">Hier klicken, um das Löschen abzulehnen!</a><br />';
    $lstrMailText .= '<a href="http://localhost/Portal/API/V3/index.php?action=' . $lstrActionApproved . '">Hier klicken, um das Löschen zu bestätigen!</a><br />';
    $lstrMailText .= "<br /><br />";
    $lstrMailText .= "<sup>Dieser Nachricht wurde automatisiert erstellt!</sup>";

    mail::send("Terminverwaltung: Neuer Termin", $lstrMailText, $ChangerEmail, null, array('ter@berzirk-hh-sued.de', 'Terminverwaltung'));

    return $larrReturnData['new_id'];
  }

  function MailTextAppointment()
  { }

  function MailText($Summary, $DTStart, $DTEnd, $Description, $Categories, $Location)
  {

    $lstrMailText = "";

    $lstrMailText .= "<h3>Termin</h3>";
    $lstrMailText .= "<hr />";
    $lstrMailText .= "Termin: " . $Summary . "<br />";
    $lstrMailText .= "Start: " . $DTStart . "<br />";
    if ($DTEnd != '') $lstrMailText .= "Ende: " . $DTEnd . "<br />";
    $lstrMailText .= "Ort: " . $Location . "<br />";
    $lstrMailText .= "Beschreibung: " . $Description . "<br />";
    $lstrMailText .= "Kategorie: " . $Categories . "<br />";
    $lstrMailText .= "<br /><br />";
    
  }

  function newAppointment($Location, $Summary, $Description, $DTStart, $DTEnd, $Categories, $ChangerName, $ChangerEmail)
  {
    global $app;

    //INSERT INTO `sched_Appointments` (`last_modified`, `status`, `version`, `changer_name`, `changer_email`, `active`, `creation_date`) VALUES (NULL, NULL, 'a location', 'a summary', 'a description', NULL, '2019-08-23 10:21:00', '2019-08-31 11:00:00', '0000-00-00 00:00:00.000000', 'a category', '0000-00-00 00:00:00.000000', 'requestCreation', '1', 'Christian Köster', 'kirche@familie-koester.eu', '1', CURRENT_TIMESTAMP);
    $lstrQuery = "INSERT INTO `sched_Appointments` (`location`, `summary`, `description`, `dtstart`, `dtend`, `categories`, `changer_name`, `changer_email`, `last_modified`, `status`)";
    $lstrQuery .= " VALUES ('" . $Location . "', '" . $Summary . "', '" . $Description . "', '" . $DTStart . "', '" . $DTEnd . "', '" . $Categories . "', '" . $ChangerName . "', '" . $ChangerEmail . "', '" . date('Y-m-d H:i:s') . "', 'requestCreation')";
    $lintID = $app->DB->execute($lstrQuery, 'id');

    if ($lintID > 0) {
      $lstrActionID = actions::create(array(
        'type' => 'func',
        'controller' => 'scheduling',
        'method' => 'deleteFromMail',
        'values' => array('id' => $lintID)
      ));

      $lstrMailText = "Hallo " . $ChangerName . "<br /><br />";
      $lstrMailText .= "Es wurde mit Deiner E-Mail-Adresse ein neuer Termin erstellt. Bitte pr&uuml;fe, ob dieser Termin von Dir erstellt wurde. Wenn Du diesen Termin erstellt hast, so brauchst Du nichts zu unternehmen. Solltest Du es nicht gewesen sein, der diesen Termin erstellt hat und Du nicht mit dem Termin einverstanden ist, so l&ouml;sche den Termin bitte. Folgender Termin wurde erstellt:";
      $lstrMailText .= "<h3>Termin</h3>";
      $lstrMailText .= "<hr />";
      $lstrMailText .= "Termin: " . $Summary . "<br />";
      $lstrMailText .= "Start: " . $DTStart . "<br />";
      if ($DTEnd != '') $lstrMailText .= "Ende: " . $DTEnd . "<br />";
      $lstrMailText .= "Ort: " . $Location . "<br />";
      $lstrMailText .= "Beschreibung: " . $Description . "<br />";
      $lstrMailText .= "Kategorie: " . $Categories . "<br />";
      $lstrMailText .= "<br /><br />";
      $lstrMailText .= '<a href="http://localhost/Portal/API/V3/index.php?action=' . $lstrActionID . '">Hier klicken, um den Termin zu l&ouml;schen</a>';
      $lstrMailText .= "<br /><br />";
      $lstrMailText .= "<sup>Dieser Nachricht wurde automatisiert erstellt!</sup>";

      mail::send("Terminverwaltung: Neuer Termin", $lstrMailText, $ChangerEmail, null, array('ter@berzirk-hh-sued.de', 'Terminverwaltung'));

      return $lintID;
    }
  }
  function sendappointment($Data)
  {
    $larrData = array();
    array_push($larrData, array(
      'Das Datum des Termins (Beginn) bitte im Format TT.MM.JJJJ eingeben, z.B. 19.01.2018.',
      'zur freien Verfügung',
      'Die Uhrzeit des Termins (Beginn) bitte im Format hh:mm angeben, z.B. 08:00.',
      'Gibt hier bitte den Titel des Termins, der Veranstaltung an. Zur besseren Lesbarkeit fürgt bitte sonstige Beschreibungen zu dem Termin in die Bemerkungen. Optimal wäre eine maximale Textlänge von 75 Zeichen, aber längere Texte sind auch möglich. Texte, die länger als 75 Zeichen sind, werden automatisch hellblau hinterlegt.',
      'Gebt hier bitte den Ort der Veranstaltung bekannt',
      'Bitte wählt soweit möglich aus den existierenden Kategorien aus. Diese sind im DropDown hinterlegt. Sollte aber die benötigte Kategorie nicht dabei sein, schreibt den benötigten Text - zur besseren erkennbarkeit wird eine so erstellte neue Kategorie automatisch hellblau hinterlegt.',
      'Bemerkungen in beliebiger Länge können hier eingefügt werden.',
      'Das geplante Ende sofern bekannt bitte im Format TT.MM.JJJJ hh:mm angeben, z.B. 19.01.2018 20:15.',
      'Teilt bitte mit, ob dies ein neuer, ein geänderter oder ein zu löschender Termin ist.',
      'Falls dies eine Terminänderung ist, bitte den alten Termin angeben'
    ));
    array_push($larrData, array(
      'Datum',
      'Zusatz',
      'Uhrzeit',
      'Thema',
      'Ort',
      'Kategorie',
      'Bemerkung',
      'geplantes Ende',
      'Aktion',
      'Alter Termin'
    ));

    foreach ($Data['Data'] as $larrAppointmentData) {
      $lstrEndData = $larrAppointmentData['DTEND'] ? date('d.m.Y G:i', \strtotime($larrAppointmentData['DTEND'])) : '';
      $lstrTime = $larrAppointmentData['DTSTART'] ? date('G:i', \strtotime($larrAppointmentData['DTSTART'])) : '00:00';
      array_push($larrData, array(
        date('d.m.Y', \strtotime($larrAppointmentData['DTSTART'])),
        $larrAppointmentData['freeText'],
        $lstrTime,
        $larrAppointmentData['SUMMARY'],
        $larrAppointmentData['LOCATION'],
        $larrAppointmentData['CATEGORIES'],
        $larrAppointmentData['DESCRIPTION'],
        $lstrEndData,
        $larrAppointmentData['action'],
        $larrAppointmentData['reference']
      ));
    }

    include_once './helper/xlsxwriter.class.php';

    $lstrFileName = './Data/Scheduling/web' . date("dmYHis") . '.xlsx';

    $writer = new \XLSXWriter();
    $writer->writeSheet($larrData);
    $writer->writeToFile($lstrFileName);


    //Load Composer's autoloader
    require 'helper/vendor/autoload.php';

    $mail = new PHPMailer();                              // Passing `true` enables exceptions
    try {
      //Server settings
      $mail->SMTPDebug = 0;                                 // Enable verbose debug output
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'w0131afa.kasserver.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'm04adf11';                 // SMTP username
      $mail->Password = 'Nak123!';                           // SMTP password
      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;                                    // TCP port to connect to

      //Recipients
      $mail->setFrom('ter@bezirk-hh-sued.de', 'Internetseite');

      $mail->addAddress('kirche@familie-koester.eu', 'Christian Köster');
      //$mail->addAddress(' Termine.HH-Sued@menne.biz', 'Jan Menne');     // Add a recipient
      $mail->addReplyTo(($larrParameter[1]['Information']['SenderMail'] != '') ? $larrParameter[1]['Information']['SenderMail'] : '', ($larrParameter[1]['Information']['SenderName'] != '') ? $larrParameter[1]['Information']['SenderName'] : '');

      //Attachments
      $mail->addAttachment($lstrFileName);         // Add attachments

      $lstrSender = ($larrParameter[1]['Information']['SenderName'] != '') ? $larrParameter[1]['Information']['SenderName'] : 'Wurde nicht angegeben';
      $lstrAdresse = ($larrParameter[1]['Information']['SenderMail'] != '') ? $larrParameter[1]['Information']['SenderMail'] : 'Wurde nicht angegeben';

      //Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = 'Terminanlieferung an Bezirk';
      $mail->Body    = 'Hallo Jan! <br /> Diese Terminanlieferung kommt aus dem Internetformular. <br /> Absender ist: <b>' . $lstrSender . '</b><br />Absender-Adresse ist: <b>' . $lstrAdresse . '</b><br />Text: ' . $larrParameter[1]['Information']['SenderText'];

      $mail->send();
      return 'Message has been sent';
    } catch (Exception $e) {
      return 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
    }
  }
}
