<?php
namespace controller;

class services {
  private $cstrDataPath = '..' . DS . '..' . DS . 'Data' . DS . 'Gottesdienste' . DS;
  function newservices($Type, $Year, $Month, $Name = null) {
    switch ($Type) {
    case 'bezirksuebergreifend':
      $lstrPathFile = $this->cstrDataPath . $Year . DS . $Month . DS . $Month . '.json';
      file_put_contents($lstrPathFile, json_encode($this->newservicesAdministrativeDistrict($Year, $Month)));
      break;
    case 'bezirklich':
      $lstrPathFile = $this->cstrDataPath . $Year . DS . $Month . DS . $Name . '.json';
      file_put_contents($lstrPathFile, json_encode($this->newservicesAdministrativeDistrict($Year, $Month)));
      break;
    case 'gemeindlich':
      $lstrPathFile = $this->cstrDataPath . $Year . DS . $Month . DS . $Name . '.json';
      file_put_contents($lstrPathFile, json_encode($this->newservicesAdministrativeDistrict($Year, $Month)));
      break;
    }
  }
  function getAvailableYears() {
    $larrFolders = array_diff(scandir($this->cstrDataPath), array('.', '..'));
    return $larrFolders;
  }
  function getAvailableMonths($Year) {
    $larrFolders = array_diff(scandir($this->cstrDataPath . $Year), array('.', '..'));
    return $larrFolders;
  }
  function get($Year, $Month, $Item) {
    if ($Item == "Bezirksübergreifend") $Item = $Month;
    return json_encode(array("result" => json_decode(file_get_contents($this->cstrDataPath . $Year . DS . $Month . DS . $Item . '.json'))));
  }
  function getAvailableUnits($Year, $Month) {
    $larrFolders = array_diff(scandir($this->cstrDataPath . $Year . DS . $Month), array('.', '..'));
    array_splice($larrFolders,0,0);
    for($lintCounter = 0; $lintCounter < count($larrFolders); $lintCounter++)    {  
      $lstrValue = str_replace('.json','', $larrFolders[$lintCounter]);      
      if (intval($lstrValue) != 0) {
        $larrFolders[$lintCounter] = 'Bezirksübergreifend';
      } else {
        $larrFolders[$lintCounter] = $lstrValue;
      }
    }
    return $larrFolders;
  }
  function copy($SourceType, $Year, $Month, $DestinationType, $DestinationName, $SourceName = null) {
    $lstrSource = '';
    if ($SourceType == 'ueberbezirklich') {
      $lstrSource = $this->cstrDataPath.DS.$Year.DS.$Month.DS.$Month.'.json';
    } else {
      $lstrSource = $this->cstrDataPath.DS.$Year.DS.$Month.DS.$SourceName.'.json';
    }

    $lstrDestination = '';
    if($DestinationType == 'ueberbezirklich') {
      $lstrDestination = $this->cstrDataPath.DS.$Year.DS.$Month.DS.$Month.'.json';
    } else {
      $lstrDestination = $this->cstrDataPath.DS.$Year.DS.$Month.DS.$DestinationName.'.json';
    }

    if (file_exists($lstrSource)) {      
      copy ($lstrSource, $lstrDestination);
      http_response_code(200);
      return true;
    } else {
      http_response_code(400);
      return 'Konnte nicht kopieren, da die Datei nicht gefunden wurde';
    }
  }
  function save() {

  }
  function checkPublicHoliday($Timestamp) {
    $lintDay = 24 * 60 * 60;
    //Karfreitag-Ostern
    $ltsEaster = easter_date(date('Y', $Timestamp));
    $ltsGoodFriday = $ltsEaster - (2 * $lintDay);

    if (date('n', $Timestamp) == date('n', $ltsGoodFriday)) {
      //Wednesdays
      if (strtotime('-2 days', $ltsGoodFriday) == $Timestamp || strtotime('-1 day', $ltsMonthGoodFriday) == $Timestamp) {
        return $ltsGoodFriday;
      }
    }
    //LabourDay
    $lmixTemp = mktime(0, 0, 0, 5, 1, date('y'));
    //Is the labour day on a day where a week service could be
    $lmixTemp = date('N', $lmixTemp);
    switch ($lmixTemp) {
    case 3:
      return strtotime('+1 day', $Timestamp);
      break;
    case 4:
      return strtotime('-1 day', $Timestamp);
      break;
    }
    //Ascension of Christ
    $lmixTemp = $ltsEaster + (39 * $lintDay);
    $lmixTemp = date('N', $lmixTemp);
    switch ($lmixTemp) {
    case 3:
      return strtotime('+1 day', $Timestamp);
      break;
    case 4:
      return strtotime('-1 day', $Timestamp);
      break;
    }
    //Day of German Unity
    $lmixTemp = mktime(0, 0, 0, 10, 3, date('y'));
    $lmixTemp = date('N', $lmixTemp);
    switch ($lmixTemp) {
    case 3:
      return strtotime('+1 day', $Timestamp);
      break;
    case 4:
      return strtotime('-1 day', $Timestamp);
      break;
    }
    //Reformation Day
    $lmixTemp = mktime(0, 0, 0, 10, 30, date('y'));
    $lmixTemp = date('N', $lmixTemp);
    switch ($lmixTemp) {
    case 3:
      return strtotime('+1 day', $Timestamp);
      break;
    case 4:
      return strtotime('-1 day', $Timestamp);
      break;
    }
    //Christmas
    //Needs to be specified
  }
  function newservicesAdministrativeDistrict($Year, $Month) {

    $larrServices = array();
    for ($lintDayCounter = 1; $lintDayCounter <= cal_days_in_month(CAL_GREGORIAN, intval($Month), intval($Year)); $lintDayCounter++) {
      $lstrDate = $Year . (strlen($Month) == 1 ? '0' . $Month : $Month) . (strlen(strval($lintDayCounter)) == 1 ? '0' . $lintDayCounter : strval($lintDayCounter));
      switch (date('N', strtotime($lstrDate))) {
      case '3':
        $lstrDate = $this->checkPublicHoliday(strtotime($lstrDate.'T193000'));
        array_push($larrServices, array('type' => 'week', 'date' => date('Y-m-d H:i:s', $lstrDate)));
        break;
      case '7':
        $lstrDate = $this->checkPublicHoliday(strtotime($lstrDate.'T100000'));
        array_push($larrServices, array('type' => 'weekend', 'date' => date('Y-m-d H:i:s', $lstrDate)));
        break;
      }
    }
    $lstrDate = $Year . (strlen($Month) == 1 ? '0' . $Month : $Month) . '01';
    $ltsDate = strtotime('+1 Month', strtotime($lstrDate));
    $lintServiceCounter = 0;
    for ($lintDayCounter = 0; $lintDayCounter <= 20; $lintDayCounter++) {
      if ($lintDayCounter != 0) {
        $ltsDate = strtotime('+1 Day', $ltsDate);
      }

      switch (date('N', $ltsDate)) {
      case '3':
        $lstrDate = $this->checkPublicHoliday(strtotime($lstrDate.'T193000'));
        array_push($larrServices, array('type' => 'week', 'date' => date('Y-m-d H:i:s', $lstrDate)));
        $lintServiceCounter++;
        break;
      case '7':
        $lstrDate = $this->checkPublicHoliday(strtotime($lstrDate.'T100000'));
        array_push($larrServices, array('type' => 'weekend', 'date' => date('Y-m-d H:i:s', $lstrDate)));
        $lintServiceCounter++;
        break;
      }
      if ($lintServiceCounter >= 2) {
        $lintDayCounter = 21;
      }
    }
    return $larrServices;
  }

}