<?php
// Web-API V3
header('Content-type: text/html; charset=utf-8');
define('DS', DIRECTORY_SEPARATOR);

// $_POST['query'] = 'SELECT * FROM  `add_Contact` LIMIT 0 , 30';
if ($_SERVER['REQUEST_METHOD'] === 'POST' && empty($_POST)) {
  $_POST = json_decode(file_get_contents('php://input'), true);
}

if (isset($_POST['query'])) {
  include_once './sql.php';
} else {
  include_once './api.php';
}
