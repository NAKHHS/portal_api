<?php
$larrConfig = include_once './config/app.php';

$lobjConnection = new \mysqli($larrConfig['database']['host'], $larrConfig['database']['user'], $larrConfig['database']['password'], $larrConfig['database']['database']);
if ($lobjConnection->connect_errno) {
  echo "Probleme bei der Verbindung zur Datenbank";
  echo "Fehlernummer: " . $lobjConnection->connect_errno . "\n";
  echo "Fehler: " . $lobjConnection->connect_error . "\n";
  exit;
}

$lobjResult = $lobjConnection->query($_POST['query']);

$larrResult = array();

if ($lobjResult) {
    if ($lobjResult->num_rows === 0) {
      echo ('null');
    } else {
      while ($larrRow = $lobjResult->fetch_assoc()) {
        array_push($larrResult, $larrRow);
      }
    }

} else {
  echo ('Fehler: ' . $lobjConnection->error);
}

echo json_encode($larrResult);